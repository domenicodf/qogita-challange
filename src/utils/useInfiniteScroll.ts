import { useRef, useEffect, useCallback } from 'react';

type UseInfiniteScrollOptions = {
  canLoadMore: boolean;
  onLoadMore: () => void;
} & Omit<IntersectionObserverInit, 'root'>;

export default function useInfiniteScroll({
  canLoadMore,
  onLoadMore,
}: UseInfiniteScrollOptions): (target: Element | null) => void {
  const targetRef = useRef<Element | null>();

  useEffect(() => {
    const target = targetRef.current;

    if (!target) {
      return;
    }

    const observer = new IntersectionObserver(
      (entries) => {
        entries.forEach(({ isIntersecting }) => {
          if (isIntersecting && canLoadMore) {
            onLoadMore();
          }
        });
      },
      {
        rootMargin: '200px',
        threshold: 1.0,
      }
    );
    observer.observe(target);

    return () => {
      observer.unobserve(target);
      observer.disconnect();
    };
  }, [canLoadMore, onLoadMore]);

  const setTargetRef = useCallback((target: Element | null) => {
    targetRef.current = target;
  }, []);

  return setTargetRef;
}
