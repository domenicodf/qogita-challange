export function formatPrice(currency: string, value: number) {
  return new Intl.NumberFormat('EN', { style: 'currency', currency }).format(value);
}
