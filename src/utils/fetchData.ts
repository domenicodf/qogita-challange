export async function fetchData<T>(path: string): Promise<T> {
  const res = await fetch(`${process.env.NEXT_PUBLIC_BASE_API_URL}${path}`, {
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
  });
  if (res.status !== 200) {
    throw new Error(res.statusText);
  }
  return res.json();
}
