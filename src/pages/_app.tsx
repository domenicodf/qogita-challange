import { AppProps } from 'next/app';
import { SWRConfig } from 'swr';

import '../global.css';

export default function QogitaApp({ Component, pageProps }: AppProps) {
  return (
    <SWRConfig>
      <Component {...pageProps} />
    </SWRConfig>
  );
}
