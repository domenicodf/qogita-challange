import { CartItem } from 'components/Cart/Item';
import { CartList } from 'components/Cart/List';
import { CartSummary } from 'components/Cart/Summary';
import { Heading } from 'components/Layout/Heading';
import PageLayout from 'components/Layout/Page';
import { useCart } from 'store/cart';

export default function CartPage() {
  const {
    cartProducts,
    cart,
    cartOverview: { totalQuantity },
  } = useCart();

  return (
    <PageLayout>
      <Heading
        title="Your Cart"
        subTitle={
          totalQuantity > 0 ? `Products: ${cart?.items.length} / Units: ${totalQuantity.toString()}` : undefined
        }
      />
      {totalQuantity < 1 ? (
        <div>Your cart is empty</div>
      ) : (
        <div className="flex justify-between flex-col-reverse md:flex-row">
          <CartList>
            {cartProducts?.map((product) => (
              <CartItem product={product} key={product.gtin} />
            ))}
          </CartList>
          <div className="min-w-300 w-full mb-4 md:w-3/5 lg:w-1/3 md:ml-10">
            <CartSummary />
          </div>
        </div>
      )}
    </PageLayout>
  );
}
