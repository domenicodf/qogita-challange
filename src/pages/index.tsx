import { GetServerSideProps } from 'next';
import { Heading } from 'components/Layout/Heading';
import PageLayout from 'components/Layout/Page';
import { ScrollTop } from 'components/Layout/ScrollTop';
import { ProductCard } from 'components/Products/Card';
import { ProductsGrid } from 'components/Products/Grid';
import { useProducts } from 'store/products';
import { ProductsResponse, API_ENDPOINTS } from 'types';
import { fetchData } from 'utils/fetchData';
import useInfiniteScroll from 'utils/useInfiniteScroll';

export type HomePageProps = {
  fallback: {
    productsResponse: ProductsResponse;
  };
};

export default function HomePage({ fallback: { productsResponse } }: HomePageProps) {
  const { products, isValidating, loadMore, hasMore, totalCount } = useProducts({
    fallbackProductsResponse: [productsResponse],
  });

  const targetRef = useInfiniteScroll({
    onLoadMore: loadMore,
    canLoadMore: hasMore,
  });

  return (
    <PageLayout>
      <Heading title="Products" subTitle={`Results: ${totalCount}`} />
      <ProductsGrid>
        {products?.map((product) => (
          <ProductCard product={product} key={product.gtin} />
        ))}
        <section ref={targetRef}>{isValidating ? 'Loading...' : null}</section>
      </ProductsGrid>
      <ScrollTop />
    </PageLayout>
  );
}

export const getServerSideProps: GetServerSideProps = async () => {
  const productsRes = await fetchData<ProductsResponse>(API_ENDPOINTS.products);
  return {
    props: {
      fallback: {
        productsResponse: productsRes,
      },
    },
  };
};
