export type CartItem = {
  productGtin: Product['gtin'];
  quantity: number;
};

export type CartOverview = {
  totalPrice: number;
  currency: string;
  totalQuantity: number;
};

export type Cart = {
  items: CartItem[];
};

export type Product = {
  name: string;
  gtin: string;
  recommendedRetailPrice: number;
  recommendedRetailPriceCurrency: string;
  imageUrl: string;
  brandName: string;
  categoryName: string;
};

/**
 * The response type of errors from /api/*.
 */
export type ErrorResponse = string;

/**
 * The response type of /api/products
 */
export type ProductsResponse = {
  count: number;
  page: number;
  results: Product[];
};

/**
 * The response type of /api/products/[gtin].
 */
export type ProductResponse = Product;

export enum API_ENDPOINTS {
  products = '/api/products',
}
