import { ReactNode } from 'react';
import { Header } from '../Header';

type PageLayoutProps = {
  children: ReactNode;
};

export default function PageLayout({ children }: PageLayoutProps) {
  return (
    <>
      <Header />
      <main className="container mx-auto pt-20 px-2">{children}</main>
    </>
  );
}
