import { ShoppingCartIcon } from '@heroicons/react/solid';
import Link from 'next/link';
import { useCart } from 'store/cart';
import { formatPrice } from 'utils/intl';

export function Header() {
  const {
    cartOverview: { totalPrice, totalQuantity, currency },
  } = useCart();

  return (
    <section className="p-2 border border-gray-100 fixed w-full bg-white/80 top-0 z-10 backdrop-blur-xl">
      <header className="flex justify-between container mx-auto items-center">
        <Link href="/">
          <a className="text-white rounded-full text-xl font-semibold px-4 py-1 bg-violet-700 hover:bg-violet-800">
            Qogita
          </a>
        </Link>
        <nav>
          <ul className="flex gap-2 items-center font-semibold">
            <li>
              <Link href="/">
                <a className="hover:bg-gray-100 transition-all px-2 py-2 rounded-xl">Products</a>
              </Link>
            </li>
            <li>
              <Link href="/cart">
                <a>
                  <div className="relative px-2 py-1 rounded-xl flex flex-col items-center hover:bg-gray-100 transition-all">
                    <ShoppingCartIcon className="h-8 w-8 " />
                    {totalQuantity > 0 && (
                      <div className="bg-violet-700 rounded-full absolute -top-0 -right-1 leading-none text-xs text-white px-2 py-1">
                        {totalQuantity}
                      </div>
                    )}
                    <span className={`text-xs font-normal ${totalPrice > 0 ? 'text-violet-700' : 'text-gray-400'} `}>
                      {formatPrice(currency, totalPrice)}
                    </span>
                  </div>
                </a>
              </Link>
            </li>
          </ul>
        </nav>
      </header>
    </section>
  );
}
