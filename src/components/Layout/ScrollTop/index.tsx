import { ArrowCircleUpIcon } from '@heroicons/react/outline';
import { useCallback } from 'react';

export function ScrollTop() {
  const handleScrollTop = useCallback(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <button className="fixed bottom-4 right-4 bg-white rounded-full" onClick={handleScrollTop}>
      <ArrowCircleUpIcon className="w-14 h-14 hover:text-violet-400 transition-all text-gray-300" />
    </button>
  );
}
