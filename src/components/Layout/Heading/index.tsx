type HeadingProps = {
  title: string;
  subTitle?: string;
};

export function Heading({ title, subTitle }: HeadingProps) {
  return (
    <div className="my-6">
      <h1 className="text-2xl font-bold">{title}</h1>
      {subTitle && <h4 className="text-md text-gray-500">{subTitle}</h4>}
    </div>
  );
}
