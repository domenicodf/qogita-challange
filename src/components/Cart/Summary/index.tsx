import { useCart } from 'store/cart';
import { formatPrice } from 'utils/intl';

export function CartSummary() {
  const {
    cartOverview: { totalPrice, currency },
  } = useCart();
  return (
    <section className="flex flex-col items-end bg-violet-50 rounded-md p-4">
      <p className="text-2xl">Est. Total</p>
      <h4 className="text-4xl font-bold">{formatPrice(currency, totalPrice)}</h4>
    </section>
  );
}
