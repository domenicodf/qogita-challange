import { ReactNode } from 'react';

type CartListProps = {
  children: ReactNode;
};

export function CartList({ children }: CartListProps) {
  return <section className="flex flex-col flex-auto">{children}</section>;
}
