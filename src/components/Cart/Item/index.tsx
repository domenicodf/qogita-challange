import Image from 'next/image';
import { ChangeEvent, FormEvent, useCallback, useEffect, useId, useMemo, useState } from 'react';
import { useCart } from 'store/cart';
import { Product } from 'types';
import { formatPrice } from 'utils/intl';

export type CartItemProps = {
  product: Product;
};
export function CartItem({ product }: CartItemProps) {
  const quantityId = useId();
  const { updateProductQuanity, removeProduct, getQuantityByProduct } = useCart();

  const quantity = useMemo(() => {
    return getQuantityByProduct(product.gtin);
  }, [getQuantityByProduct, product.gtin]);

  const [quantityValue, setQuantityValue] = useState<string | number>(quantity);

  useEffect(() => {
    setQuantityValue(quantity);
  }, [quantity]);

  const handleQuantityFieldUpdate = useCallback((evt: ChangeEvent<HTMLInputElement>) => {
    const value = evt.currentTarget.value;
    setQuantityValue(value);
  }, []);

  const handleQuantitySubmit = useCallback(
    (evt: FormEvent<HTMLFormElement>) => {
      evt.preventDefault();
      let newQuantity: number;
      if (typeof quantityValue === 'string') {
        newQuantity = parseInt(quantityValue);
      } else {
        newQuantity = quantityValue;
      }
      updateProductQuanity(product.gtin, newQuantity);
    },
    [product, quantityValue, updateProductQuanity]
  );

  const handleRemoveProduct = useCallback(() => {
    removeProduct(product.gtin);
  }, [product.gtin, removeProduct]);

  return (
    <article className="border-b border-gray-200 flex flex-col relative py-4 flex-auto mb-4">
      <div className="flex">
        <div className="flex-initial">
          <Image src={product.imageUrl} alt={product.name} quality={100} height={64} width={62} objectFit="contain" />
        </div>
        <div className="ml-6 flex justify-between flex-grow">
          <div>
            <h5 className="text-lg">{product.name}</h5>
            <p className="text text-sm text-gray-800">{product.gtin}</p>
          </div>
          <div className="flex flex-col items-end ml-4">
            <span className="text-xl font-semibold">
              {formatPrice(product.recommendedRetailPriceCurrency, product.recommendedRetailPrice * quantity)}
            </span>
            <span className="text text-gray-500">
              {formatPrice(product.recommendedRetailPriceCurrency, product.recommendedRetailPrice)}
            </span>
          </div>
        </div>
      </div>
      <div className="flex mt-2">
        <button
          type="button"
          className="text-sm px-2 py-1 border border-gray-100 rounded-md hover:text-red-500"
          onClick={handleRemoveProduct}
        >
          Remove
        </button>
        <div className="flex items-center ml-4">
          <form onSubmit={handleQuantitySubmit}>
            <label htmlFor={quantityId} className="text-sm mr-1">
              Quantity:
            </label>
            <input
              className="bg-gray-50 text-sm rounded-md px-2 py-1 w-20 border border-gray-200 text-right"
              id={quantityId}
              type="number"
              value={quantityValue}
              min={1}
              onChange={handleQuantityFieldUpdate}
            />
            {quantity !== quantityValue && (
              <button
                type="submit"
                className="text-sm px-2 py-1 text-violet-600 border border-gray-200 rounded-md hover:text-purple-500 ml-2"
              >
                Update
              </button>
            )}
          </form>
        </div>
      </div>
    </article>
  );
}
