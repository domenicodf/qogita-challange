import { ReactNode } from 'react';

type ProductsGridProps = {
  children: ReactNode;
};

export function ProductsGrid({ children }: ProductsGridProps) {
  return (
    <section className="grid gap-2 xl:grid-cols-4 lg:grid-cols-3 md:grid-cols-2 sm:grid-cols-2">{children}</section>
  );
}
