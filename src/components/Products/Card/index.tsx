import { ShoppingCartIcon } from '@heroicons/react/outline';
import Image from 'next/image';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { useCart } from 'store/cart';
import { Product } from 'types';
import { formatPrice } from 'utils/intl';

export type ProductCardProps = {
  product: Product;
};
export function ProductCard({ product }: ProductCardProps) {
  const { addProduct, isValidating, getQuantityByProduct } = useCart();
  const [isAddingToCart, setIsAddingToCart] = useState(isValidating);

  const quantity = useMemo(() => {
    return getQuantityByProduct(product.gtin);
  }, [getQuantityByProduct, product.gtin]);

  const handleAddProductToCart = useCallback(() => {
    setIsAddingToCart(true);
    addProduct(product.gtin);
  }, [addProduct, product]);

  useEffect(() => {
    if (isAddingToCart && !isValidating) {
      setTimeout(() => {
        setIsAddingToCart(false);
      }, 250);
    }
  }, [isAddingToCart, isValidating]);

  return (
    <article className="border border-gray-200 rounded flex flex-col relative py-4 px-4">
      <Image src={product.imageUrl} alt={product.name} quality={100} height={192} width={192} objectFit="contain" />
      <div className="mt-6 flex flex-col flex-grow">
        <div className="flex-grow">
          <h5 className="text-lg">{product.name}</h5>
          <p className="text text-gray-600">{product.brandName}</p>
          <p className="mt-2">
            <span className="text-sm bg-gray-200 px-2 py-1 rounded-full">{product.categoryName}</span>
          </p>
        </div>
        <div className="flex-initial mt-4 flex justify-between items-end">
          <span className="text-xl font-bold flex items-center mb-1">
            {formatPrice(product.recommendedRetailPriceCurrency, product.recommendedRetailPrice)}
          </span>
          <div className="flex flex-col items-end justify-end">
            <div className="text-purple-500 ml-1 text-sm font-normal h-5 w-full text-right ">
              {quantity > 0 && `${quantity} in Cart`}
            </div>
            <button
              onClick={handleAddProductToCart}
              className={`flex items-center text-violet-600 border border-gray-200 px-2 py-1 rounded-md hover:bg-violet-600 hover:text-white hover:border-violet-900 transition-all disabled:opacity-20`}
              disabled={isAddingToCart}
            >
              <ShoppingCartIcon className="h-6 w-6 mr-1" />
              <span className="text-sm">Add to Cart</span>
            </button>
          </div>
        </div>
      </div>
    </article>
  );
}
