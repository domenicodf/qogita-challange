import { useCallback, useMemo } from 'react';
import useSWRInfinite, { SWRInfiniteKeyLoader } from 'swr/infinite';
import { API_ENDPOINTS, Product, ProductsResponse } from 'types';
import { fetchData } from 'utils/fetchData';

const getProductsSWRKey: SWRInfiniteKeyLoader = (page, prevPageData: ProductsResponse) => {
  let pageQuery = '';
  if (page) {
    pageQuery = `?page=${++page}`;
  }

  if (prevPageData && prevPageData.count / prevPageData.page === prevPageData.results.length) {
    return null;
  }

  return `${API_ENDPOINTS.products}${pageQuery}`;
};

type UseProductsOptions = {
  fallbackProductsResponse: ProductsResponse[];
};

export function useProducts({ fallbackProductsResponse }: UseProductsOptions) {
  const { data, error, isValidating, size, setSize } = useSWRInfinite<ProductsResponse>(getProductsSWRKey, fetchData, {
    initialSize: 1,
    revalidateAll: false,
    revalidateFirstPage: false,
    revalidateIfStale: false,
    revalidateOnMount: false,
    revalidateOnReconnect: false,
    revalidateOnFocus: false,
    fallbackData: fallbackProductsResponse,
  });

  const totalCount = useMemo(() => {
    return data ? data[0].count : 0;
  }, [data]);

  const products = useMemo(() => {
    const initialProducts: Product[] = [];
    return data?.reduce((prevProducts, { results: currentRes }) => {
      return [...prevProducts, ...currentRes];
    }, initialProducts);
  }, [data]);

  const hasMore = useMemo(() => {
    if (products) {
      return products.length < totalCount;
    }
    return false;
  }, [products, totalCount]);

  const loadMore = useCallback(() => {
    void setSize(size + 1);
  }, [setSize, size]);

  return {
    products,
    loadMore,
    hasMore,
    isValidating,
    error,
    totalCount,
  };
}
