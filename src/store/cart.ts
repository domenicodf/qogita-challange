import { useCallback, useMemo } from 'react';
import { mutate } from 'swr';
import useSWRImmutable from 'swr/immutable';
import { API_ENDPOINTS, Cart, CartItem, CartOverview, Product, ProductResponse } from 'types';
import { fetchData } from 'utils/fetchData';

const CART_KEY = 'cart';

function cartFetcher(key: string): Cart {
  const cart = localStorage.getItem(key);
  if (cart) {
    return JSON.parse(cart);
  }
  return {
    items: [],
  };
}

async function cartProductsFetcher(cart: Cart): Promise<Product[]> {
  return Promise.all(
    cart.items.map(async ({ productGtin }) => {
      const path = `${API_ENDPOINTS.products}/${productGtin}`;
      return mutate(path, async (cartProduct: ProductResponse) => {
        if (cartProduct) {
          return cartProduct;
        }
        return fetchData<ProductResponse>(path);
      });
    })
  );
}

export function useCart() {
  const { data: cart, mutate: mutateCart, isValidating: cartValidating } = useSWRImmutable(CART_KEY, cartFetcher);
  const { data: cartProducts, isValidating: cartProductsValidating } = useSWRImmutable<Product[]>(
    cart,
    cart ? cartProductsFetcher : null
  );

  const updateCart = useCallback(
    (newCart: Cart) => {
      if (!cart) {
        return;
      }

      localStorage.setItem(CART_KEY, JSON.stringify(newCart));
      void mutateCart(newCart);
    },
    [cart, mutateCart]
  );

  const removeProduct = useCallback(
    (productGtin: Product['gtin']) => {
      if (!cart) {
        return;
      }

      cart.items = cart.items.filter((cartItem) => {
        return cartItem.productGtin !== productGtin;
      });

      updateCart({ ...cart });
    },
    [cart, updateCart]
  );

  const updateProductQuanity = useCallback(
    (productGtin: Product['gtin'], quantity: number) => {
      if (!cart) {
        return;
      }

      cart.items = cart.items.map((cartItem) => {
        if (cartItem.productGtin === productGtin) {
          return {
            ...cartItem,
            quantity,
          };
        }
        return cartItem;
      });

      updateCart({ ...cart });
    },
    [cart, updateCart]
  );

  const addProduct = useCallback(
    (productGtin: Product['gtin']) => {
      if (!cart) {
        return;
      }

      const currentCartItem = cart?.items.find((cartItem) => cartItem.productGtin === productGtin);

      if (currentCartItem) {
        return updateProductQuanity(currentCartItem.productGtin, currentCartItem.quantity + 1);
      }

      const newCartItem: CartItem = {
        productGtin,
        quantity: 1,
      };

      cart.items = [...cart.items, newCartItem];
      updateCart({ ...cart });
    },
    [cart, updateCart, updateProductQuanity]
  );

  const totalQuantity = useMemo(() => {
    if (!cart) {
      return 0;
    }
    return cart.items.reduce((prevQuantity, { quantity }) => {
      return prevQuantity + quantity;
    }, 0);
  }, [cart]);

  const cartOverview: CartOverview = useMemo(() => {
    if (!cartProducts || !cartProducts.length || !cart) {
      return {
        totalPrice: 0,
        currency: 'EUR',
        totalQuantity: 0,
      };
    }
    const totalPrice = cart.items.reduce((prevQuantity, { quantity, productGtin }) => {
      const cartProduct = cartProducts.find((product) => product.gtin === productGtin);
      if (!cartProduct) {
        return prevQuantity;
      }
      return prevQuantity + quantity * cartProduct.recommendedRetailPrice;
    }, 0);

    return {
      totalPrice: totalPrice,
      currency: cartProducts[0].recommendedRetailPriceCurrency,
      totalQuantity,
    };
  }, [cart, cartProducts, totalQuantity]);

  const getQuantityByProduct = useCallback(
    (productGtin: Product['gtin']) => {
      const cartItem = cart?.items.find((item) => item.productGtin === productGtin);
      if (!cartItem) {
        return 0;
      }
      return cartItem.quantity;
    },
    [cart?.items]
  );

  return {
    cart,
    addProduct,
    removeProduct,
    updateProductQuanity,
    totalQuantity,
    cartOverview,
    cartProducts,
    isValidating: cartValidating && cartProductsValidating,
    getQuantityByProduct,
  };
}
